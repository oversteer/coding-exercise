
package app;

import static app.Roulette.PLAYERS_DATA_FILE;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * @author Brendan Healey
 */

public final class SpinTheWheelThread implements Runnable {

    private static final int MIN_ROULETTE_NUMBER = 0;
    private static final int MAX_ROULETTE_NUMBER = 36;
    private static final int EVEN_ODDS_WIN_MULTIPLIER = 2;
    private static final int NUMBER_WIN_MULTIPLIER = 36;
    
    private List<PlayerDetail> playerDetailsList;
    private List<Bet> betQueue;
    private int interval;
    private List<Bet> auditLog;
    
    private Random generator = new Random();
    
    public SpinTheWheelThread(final List<PlayerDetail> playerDetailsList,
            final List<Bet> betQueue, int interval, List<Bet> auditLog) {
        
        this.playerDetailsList = playerDetailsList;
        this.betQueue = betQueue;
        this.interval = interval;
        this.auditLog = auditLog;
    }

    @Override
    public void run() {
        
        while(true) {
            
            try {
                Thread.sleep(interval);
                
                spinTheWheel(betQueue);
                
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
    }

    private void spinTheWheel(List<Bet> betQueue) {

        List<Bet> betQueueLocalCopy = null;
        
        synchronized(betQueue) {
            
            betQueueLocalCopy = new ArrayList<>(betQueue);
            betQueue.clear();
        }
        
        final int winningNumber = generateTheWinningNumber();
        
        processPlacedBets(betQueueLocalCopy, winningNumber);
        
        updatePlayerTotals(betQueueLocalCopy);

        flushPlayerTotals();

        if(auditLog != null) {
            synchronized(auditLog) {
                auditLog.addAll(betQueueLocalCopy);
            }
        }

    }

    private void processPlacedBets(final List<Bet> betQueueLocalCopy, final int winningNumber) {
    
        System.out.println("Number: " + winningNumber);
        System.out.println("Player\tBet\tOutcome\tWinnings");
        
        for(Bet bet : betQueueLocalCopy) {
            processSingleBet(bet, winningNumber);
            printBetResult(bet);
        }
        
    }

    private void processSingleBet(Bet bet, final int winningNumber) {

        if(calcOutcome(bet, winningNumber) == Bet.BetResult.WIN) {
            calcWinnings(bet);
        }
        else {
            bet.setWinnings(BigDecimal.ZERO);
        }
    }

    private void printBetResult(final Bet bet) {
        
        StringBuilder sb = new StringBuilder();
        
        sb.append(bet.getPlayerName()).append("\t");
        sb.append(bet.getAmount()).append("\t");
        sb.append(bet.getResult().name()).append("\t");
        sb.append(bet.getWinnings());

        System.out.println(sb.toString());
    }

    public static Bet.BetResult calcOutcome(Bet bet, final int winningNumber) {

        Bet.BetResult result = Bet.BetResult.LOSE;
        
        switch(bet.getType()) {
                
            case EVEN:
                if((winningNumber & 1) == 0) {
                    result = Bet.BetResult.WIN;
                }
                break;

            case ODD:
                if((winningNumber & 1) != 0) {
                    result = Bet.BetResult.WIN;
                }
                break;

            case NUMBER:
                if(bet.getNumber() == winningNumber) {
                    result = Bet.BetResult.WIN;
                }
                break;

            default:
                System.out.println("unknown bet type: " + bet.getType());
        }
        
        bet.setResult(result);
        
        return result;
    }
    
    public static void calcWinnings(Bet bet) {

        BigDecimal winnings = new BigDecimal(0);
        
        if(bet.getResult() == Bet.BetResult.LOSE) {
            bet.setWinnings(winnings);
            return;
        }
        
        switch(bet.getType()) {

            case EVEN:
            case ODD:
                winnings = bet.getAmount().multiply(new BigDecimal(EVEN_ODDS_WIN_MULTIPLIER));
                break;

            case NUMBER:
                winnings = bet.getAmount().multiply(new BigDecimal(NUMBER_WIN_MULTIPLIER));
                break;

            default:
                System.out.println("unknown bet type: " + bet.getType());
        }

        bet.setWinnings(winnings);
    }
    
    private int generateTheWinningNumber() {
        
        /*
         * Not necessary from a structural perspective, but spell out what we're doing,
         * it's important.
         */
        
        return randomInt();
    }
    
    private int randomInt() {
        
        /*
         * Not strictly necessary to + 0 because it will always be 0, however if this was used elsewhere...
         */
        
        int i = generator.nextInt(MAX_ROULETTE_NUMBER - MIN_ROULETTE_NUMBER + 1) + MIN_ROULETTE_NUMBER;
        return i;
    }

    private void updatePlayerTotals(final List<Bet> bets) {
        
        for(Bet bet : bets) {
            if(bet.getResult() == Bet.BetResult.WIN) {
                for(PlayerDetail player : playerDetailsList) {
                    if(player.getName().equals(bet.getPlayerName())) {
                        player.setWinTotal(player.getWinTotal().add(bet.getWinnings()));
                    }
                }
            }
        }
    }
    
    private void flushPlayerTotals() {
        
        try {
            ConfigFileManager.savePlayerDetails(PLAYERS_DATA_FILE, playerDetailsList);
        } catch (FileNotFoundException ex) {
            System.out.println("Can't find file");
        } catch (IOException ex) {
            System.out.println("Unable to read file");
        }
    }
    
}
