
package app;

import java.math.BigDecimal;

/*
 * @author Brendan Healey
 */

public class PlayerDetail {

    private String name;
    private BigDecimal winTotal;
    private BigDecimal betTotal;

    public PlayerDetail(String lineFromFile) {
        
        String[] parts = lineFromFile.split(",");
        
        if(parts.length != RouletteClient.PLAYERS_DATA_FILE_NUM_TOKENS) {
            throw new IllegalArgumentException("the file is in an invalid format");
        }
        
        try {
            this.name = parts[0];
            this.winTotal = new BigDecimal(parts[1]);
            this.betTotal = new BigDecimal(parts[2]);
        
        } catch(NumberFormatException e) {
            throw new IllegalArgumentException("the file is in an invalid format");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getWinTotal() {
        return winTotal;
    }

    public void setWinTotal(BigDecimal winTotal) {
        this.winTotal = winTotal;
    }

    public BigDecimal getBetTotal() {
        return betTotal;
    }

    public void setBetTotal(BigDecimal betTotal) {
        this.betTotal = betTotal;
    }
    
}
