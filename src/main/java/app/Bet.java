
package app;

import java.math.BigDecimal;

/*
 * @author Brendan Healey
 */

public final class Bet {

    // Dependencies exist on the names of these enum values - don't change them
    public static enum BetType { ODD, EVEN, NUMBER };
    public static enum BetResult { WIN, LOSE };
    
    private String playerName;
    private BetType type;
    private int number;
    private BigDecimal amount;
    
    private BetResult result;
    private BigDecimal winnings;

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public BetType getType() {
        return type;
    }

    public void setType(BetType type) {
        this.type = type;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BetResult getResult() {
        return result;
    }

    public void setResult(BetResult result) {
        this.result = result;
    }

    public BigDecimal getWinnings() {
        return winnings;
    }

    public void setWinnings(BigDecimal winnings) {
        this.winnings = winnings;
    }

}
