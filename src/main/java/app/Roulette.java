package app;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
 * @author Brendan Healey
 */
public final class Roulette {

    public static final String PLAYERS_DATA_FILE = "C:\\Users\\Brendan\\roulettePlayerData.dat";
    public static final int DEFAULT_INTERVAL = 30 * 1000;

    private RouletteClient client;
    
    public static void main(final String[] args) {
        new Roulette().process();
    }
    
    private void process() {

        List<PlayerDetail> playerDetailsList = new ArrayList<>();
        List<Bet> betQueue = new ArrayList<>();

        client = new RouletteClient(playerDetailsList, betQueue, PLAYERS_DATA_FILE);
        
        try {

            client.loadPlayerDataFile(PLAYERS_DATA_FILE);

            startSpinTheWheelThread(playerDetailsList, betQueue, DEFAULT_INTERVAL, null);

            client.acceptBetsLoop();

        } catch (FileNotFoundException e) {
            System.out.println("Can't find file");
        } catch (IOException e) {
            System.out.println("Unable to read file");
        }
    }

    public static void startSpinTheWheelThread(final List<PlayerDetail> playerDetailsList,
            final List<Bet> betQueue, int interval, final List<Bet> betAuditList) {

        SpinTheWheelThread r = new SpinTheWheelThread(playerDetailsList, betQueue, interval, betAuditList);
        Thread t = new Thread(r);

        t.start();
    }

}
