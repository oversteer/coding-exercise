
package app;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.List;

/*
 * @author Brendan Healey
 */

public class RouletteClient {

    public static final int PLAYERS_DATA_FILE_NUM_TOKENS = 3;
    public static final int BET_INPUT_NUM_TOKENS = 3;
    
    private List<PlayerDetail> playerDetailsList;
    private List<Bet> betQueue;
    private String filename;
    
    
    public RouletteClient(List<PlayerDetail> playerDetailsList, List<Bet> betQueue, String filename) {
        this.playerDetailsList = playerDetailsList;
        this.betQueue = betQueue;
        this.filename = filename;
    }
    
    public void loadPlayerDataFile(String filename) throws FileNotFoundException, IOException {

        List<String> playersListFromFile = ConfigFileManager.getPlayerDetails(filename);

        for (String lineFromFile : playersListFromFile) {
            PlayerDetail playerDetail = new PlayerDetail(lineFromFile);
            
            synchronized(playerDetailsList) {
                playerDetailsList.add(playerDetail);
            }
        }

    }
    
    public Bet parseBet(final String input) {
        
        // Tiki_Monkey 2 1.0
        
        String[] parts = input.split(" ");
        
        if(parts.length != BET_INPUT_NUM_TOKENS) {
            System.out.println("the input is in an invalid format");
            return null;
        }

        String name = parts[0];
        if(!checkName(name)) {
            System.out.println("the player is not in the data file");
            return null;
        }
        
        String type = parts[1];
        if(!checkType(type)) {
            return null;
        }
        
        String strAmount = parts[2];
        BigDecimal amount = checkAmount(strAmount);
        if(amount == null) {
            return null;
        }
        
        Bet newBet = new Bet();
        newBet.setPlayerName(name);
        setupTypeOrNumber(newBet, type);
        newBet.setAmount(amount);
        
        return newBet;
    }

    private void setupTypeOrNumber(Bet bet, final String typeOrNumber) {
    
        if(typeOrNumber.equals(Bet.BetType.ODD.name())) {
            bet.setType(Bet.BetType.ODD);
        }
        else if(typeOrNumber.equals(Bet.BetType.EVEN.name())) {
            bet.setType(Bet.BetType.EVEN);
        }
        else {
            bet.setType(Bet.BetType.NUMBER);
            try {
                Integer i = Integer.parseInt(typeOrNumber);
                bet.setNumber(i);
            } catch(NumberFormatException e) {
                throw new IllegalArgumentException("the input is in an invalid format");
            }
        }
    }
    
    private boolean checkName(final String name) {
        for(PlayerDetail p : playerDetailsList) {
            synchronized(p) {
                if(p.getName().equals(name)) {
                    return true;
                }
            }
        }
        
        return false;
    }

    private boolean checkType(final String type) {
        
        if(type.equals(Bet.BetType.ODD.name()) || type.equals(Bet.BetType.EVEN.name())) {
            return true;
        }
        else {
            try {
                Integer i = Integer.parseInt(type);
                return true;
            } catch(NumberFormatException e) {
                // fall through
            }
        }
        
        return false;
    }
    
    private BigDecimal checkAmount(final String amount) {
        
        BigDecimal newAmount;
        
        try {
            newAmount = new BigDecimal(amount);
        
        } catch(NumberFormatException e) {
            
            System.out.println("the amount is in an invalid format");
            return null;
        }
        
        return newAmount;
    }
    
    public void acceptBetsLoop() {

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            String input;

            while ((input = br.readLine()) != null) {
                Bet newBet = parseBet(input);
                if(newBet != null) {
                    placeBet(input);
                    
                    printTotals(playerDetailsList);
                }
            }
            
            br.close();

        } catch (FileNotFoundException e) {
            System.out.println("Can't find file");
        } catch (IOException io) {
            System.out.println("An error occurred reading from the console");
        }

    }

    public Bet placeBet(String betString) throws FileNotFoundException, IOException {
    
        Bet newBet = parseBet(betString);
        
        if(newBet != null) {
            synchronized(betQueue) {
                betQueue.add(newBet);
            }
            updateBetTotal(playerDetailsList, newBet);
        }
        
        return newBet;
    }
    
    private void printTotals(final List<PlayerDetail> playerDetailsList) {

        System.out.println("Player\tTotal Win\tTotal Bet");
        
        for(PlayerDetail p : playerDetailsList) {
            
            String name;
            BigDecimal winTotal;
            BigDecimal betTotal;
            
            synchronized(p) {
                name = p.getName();
                winTotal = p.getWinTotal();
                betTotal = p.getBetTotal();
            }

            StringBuilder sb = new StringBuilder();
            
            sb.append(name).append("\t");
            sb.append(winTotal).append("\t");
            sb.append(betTotal);
            
            System.out.println(sb.toString());
        }
    }
    
    private void updateBetTotal(final List<PlayerDetail> playerDetailsList, final Bet newBet) throws FileNotFoundException, IOException {
        
        for(PlayerDetail p : playerDetailsList) {
            if(p.getName().equals(newBet.getPlayerName())) {
                p.setBetTotal(p.getBetTotal().add(newBet.getAmount()));
            }
        }
    
        try {
            ConfigFileManager.savePlayerDetails(filename, playerDetailsList);
        } catch (FileNotFoundException ex) {
            System.out.println("Can't find file");
        } catch (IOException ex) {
            System.out.println("Unable to read file");
        }
        
    }
}
