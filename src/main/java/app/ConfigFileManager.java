package app;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
 * @author Brendan Healey
 */

public class ConfigFileManager {

    public static List<String> getPlayerDetails(final String filename) throws FileNotFoundException, IOException {

        File f = new File(filename);

        FileReader fr = new FileReader(f);
        BufferedReader br = new BufferedReader(fr);

        String line;

        List<String> playerDetails = new ArrayList<>();
        
        while ((line = br.readLine()) != null) {
            playerDetails.add(line);
        }

        br.close();
        
        return playerDetails;
    }
    
    public static void savePlayerDetails(final String filename, List<PlayerDetail> playerDetailsList)
            throws FileNotFoundException, IOException {

        File f = new File(filename);

        FileWriter fw = new FileWriter(f);
        BufferedWriter bw = new BufferedWriter(fw);

        synchronized(playerDetailsList) {
            for(PlayerDetail p : playerDetailsList) {
                StringBuilder sb = new StringBuilder();
                sb.append(p.getName()).append(",");
                sb.append(p.getWinTotal()).append(",");
                sb.append(p.getBetTotal()).append("\n");
                bw.write(sb.toString());
            }
        }
        
        bw.close();
    }
    
}