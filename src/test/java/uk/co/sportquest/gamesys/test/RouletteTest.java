/*
 */
package uk.co.sportquest.gamesys.test;

import app.Bet;
import app.PlayerDetail;
import app.Roulette;
import app.RouletteClient;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Brendan
 */

public class RouletteTest {
    
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
    /**
     * Test of main method, of class Roulette.
     */
    
    @Test
    public void rouletteTest() {
        
        System.out.println("rouletteTest");
        
        List<PlayerDetail> playerDetailsList = new ArrayList<>();
        List<Bet> betQueue = new ArrayList<>();
        List<Bet> auditLog = new ArrayList<>();

        RouletteClient client = new RouletteClient(playerDetailsList, betQueue, Roulette.PLAYERS_DATA_FILE);
        
        try {

            client.loadPlayerDataFile(Roulette.PLAYERS_DATA_FILE);

            Roulette.startSpinTheWheelThread(playerDetailsList, betQueue, 2000, auditLog);

            for(int i = 0; i < 1000; i++) {
                client.placeBet("Barbara EVEN 3.0");
                client.placeBet("Barbara ODD 3.0");
            }
            
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                System.out.println("InterruptedException");
                Thread.currentThread().interrupt();
            }
            
            BigDecimal total = new BigDecimal(0);

            System.out.println("Audit log size: " + auditLog.size());
            
            for(Bet b : auditLog) {
                if(b.getWinnings() != BigDecimal.ZERO && !(b.getWinnings().compareTo(new BigDecimal("6.0")) == 0)) {
                    System.out.println("GOT ONE: " + b.getWinnings());
                }
                total = total.add(b.getWinnings());
            }

            int nz = 0;
            int n6 = 0;
            int nx = 0;
            
            if(total.compareTo(new BigDecimal("6000.0")) != 0) {
                for(Bet b : auditLog) {
                    if(b.getWinnings() == BigDecimal.ZERO) {
                        nz++;
                    }
                    else if(b.getWinnings().compareTo(new BigDecimal("6.0")) == 0) {
                        n6++;
                    }
                    else {
                        nx++;
                    }
                }
            }
            
            System.out.println("TOTAL WINNINGS: " + total);
            System.out.println("nz: " + nz + " n6: " + n6 + " nx: " + nx);
            
        } catch (FileNotFoundException e) {
            System.out.println("Can't find file");
        } catch (IOException e) {
            System.out.println("Unable to read file");
        }
    }
    
}
